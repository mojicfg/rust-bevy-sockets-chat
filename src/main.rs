use bevy::prelude::*;

mod net;
mod ui;

fn main() {
    App::new()
        .add_plugin(net::NetPlugin)
        .add_plugin(ui::UiPlugin)
        .run()
}
