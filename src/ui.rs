use crate::net::{ConnectEvent, Peers, RecvEvent, SendEvent};
use bevy::prelude::*;

pub struct NewPeerEvent(pub usize, pub String);
pub struct ConnectionFailedEvent;
struct InfoMessageEvent(String);

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<NewPeerEvent>()
            .add_event::<ConnectionFailedEvent>()
            .add_event::<InfoMessageEvent>()
            .add_plugins(DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "Rust Bevy Sockets Chat".to_string(),
                    resize_constraints: WindowResizeConstraints {
                        min_width: 400.0,
                        min_height: 300.0,
                        max_width: 800.0,
                        max_height: 600.0,
                    },
                    ..default()
                }),
                ..default()
            }))
            .add_startup_system(spawn_ui)
            .add_system(type_and_submit_message)
            .add_system(log_new_peer)
            .add_system(add_new_peer_to_list)
            .add_system(toggle_peers_by_click)
            .add_system(log_connection_failed)
            .add_system(log_incoming_message)
            .add_system(log_outgoing_message)
            .add_system(log_info_message);
    }
}

fn type_and_submit_message(
    mut char_evr: EventReader<ReceivedCharacter>,
    mut q: Query<&mut Text, With<MessageInput>>,
    mut submit_evw: EventWriter<SendEvent>,
    mut connect_evw: EventWriter<ConnectEvent>,
) {
    const BACKSPACE: char = '\x08';
    const RETURN: char = '\x0D';
    let mut ui_text = q.single_mut();
    for char_ev in char_evr.iter() {
        match char_ev.char {
            BACKSPACE => _ = ui_text.sections[0].value.pop(),
            RETURN => {
                let text = ui_text.sections[0].value.clone();
                ui_text.sections[0].value = String::from("");
                if text.starts_with("connect ") {
                    connect_evw.send(ConnectEvent {
                        address: text.strip_prefix("connect ").unwrap().to_string(),
                    });
                } else {
                    submit_evw.send(SendEvent { message: text });
                }
            }
            c => ui_text.sections[0].value.push(c),
        }
    }
}

fn log_new_peer(mut evr: EventReader<NewPeerEvent>, mut evw: EventWriter<InfoMessageEvent>) {
    for ev in evr.iter() {
        evw.send(InfoMessageEvent(format!("new peer connected: {}", ev.1)));
    }
}

#[derive(Component)]
struct ListOfPeersItem(usize);
fn add_new_peer_to_list(
    mut evr: EventReader<NewPeerEvent>,
    mut cmd: Commands,
    q: Query<Entity, With<ListOfPeers>>,
    font: Res<MyFont>,
) {
    let ui_parent = q.single();
    for ev in evr.iter() {
        let ui_item = cmd
            .spawn(NodeBundle {
                style: Style {
                    flex_grow: 0.0,
                    flex_shrink: 0.0,
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    ListOfPeersItem(ev.0),
                    Interaction::default(),
                    TextBundle::from_section(
                        ev.1.clone(),
                        TextStyle {
                            font: font.0.clone(),
                            font_size: 19.0,
                            color: Color::BLACK,
                        },
                    ),
                ));
            })
            .id();
        cmd.entity(ui_parent).push_children(&[ui_item]);
    }
}

fn toggle_peers_by_click(
    mut q: Query<(&mut Text, &Interaction, &ListOfPeersItem), Changed<Interaction>>,
    mut peers: ResMut<Peers>,
) {
    for (mut ui_text, interaction, list_of_peers_index) in &mut q {
        if *interaction == Interaction::Clicked {
            if ui_text.sections[0].style.color == Color::BLACK {
                ui_text.sections[0].style.color = Color::SILVER;
                peers.0[list_of_peers_index.0].0 = false;
            } else {
                ui_text.sections[0].style.color = Color::BLACK;
                peers.0[list_of_peers_index.0].0 = true;
            }
        }
    }
}

fn log_connection_failed(
    mut evr: EventReader<ConnectionFailedEvent>,
    mut evw: EventWriter<InfoMessageEvent>,
) {
    for _ in evr.iter() {
        evw.send(InfoMessageEvent("connection failed".to_string()));
    }
}

fn log_incoming_message(
    mut evr: EventReader<RecvEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            format!("from {}: ", ev.address),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.message),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::BLACK,
            },
        ));
    }
}

fn log_outgoing_message(
    mut evr: EventReader<SendEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            "you: ".to_string(),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.message),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::BLACK,
            },
        ));
    }
}

fn log_info_message(
    mut evr: EventReader<InfoMessageEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.0),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
    }
}

#[derive(Component)]
struct MessageLog;
#[derive(Component)]
struct MessageInput;
#[derive(Component)]
struct ListOfPeers;
#[derive(Resource)]
struct MyFont(Handle<Font>);
fn spawn_ui(mut cmd: Commands, asset_server: Res<AssetServer>) {
    let font_handle = asset_server.load("/usr/share/fonts/noto/NotoSans-Regular.ttf");
    cmd.insert_resource(MyFont(font_handle.clone()));
    cmd.spawn(Camera2dBundle::default());
    cmd.spawn(NodeBundle {
        background_color: BackgroundColor(Color::SILVER),
        style: Style {
            size: Size::all(Val::Percent(100.0)),
            padding: UiRect::all(Val::Px(20.0)),
            flex_direction: FlexDirection::Column,
            gap: Size::all(Val::Px(20.0)),
            ..default()
        },
        ..default()
    })
    .with_children(|parent| {
        parent
            .spawn(NodeBundle {
                style: Style {
                    flex_shrink: 1.0,
                    flex_grow: 1.0,
                    gap: Size::all(Val::Px(20.0)),
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent
                    .spawn(NodeBundle {
                        background_color: BackgroundColor(Color::WHITE),
                        style: Style {
                            padding: UiRect::all(Val::Px(10.0)),
                            flex_shrink: 1.0,
                            flex_grow: 1.0,
                            ..default()
                        },
                        ..default()
                    })
                    .with_children(|parent| {
                        parent.spawn((MessageLog, TextBundle::from_sections([])));
                    });
                parent.spawn((
                    ListOfPeers,
                    NodeBundle {
                        background_color: BackgroundColor(Color::WHITE),
                        style: Style {
                            flex_shrink: 0.0,
                            flex_grow: 0.0,
                            padding: UiRect::all(Val::Px(10.0)),
                            size: Size::width(Val::Px(150.0)),
                            flex_direction: FlexDirection::Column,
                            gap: Size::all(Val::Px(3.0)),
                            ..default()
                        },
                        ..default()
                    },
                ));
            });
        parent
            .spawn(NodeBundle {
                background_color: BackgroundColor(Color::WHITE),
                style: Style {
                    flex_shrink: 0.0,
                    flex_grow: 0.0,
                    padding: UiRect::all(Val::Px(10.0)),
                    size: Size::height(Val::Px(50.0)),
                    justify_content: JustifyContent::FlexStart,
                    align_items: AlignItems::Center,
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    MessageInput,
                    TextBundle::from_section(
                        "",
                        TextStyle {
                            font: font_handle.clone(),
                            font_size: 19.0,
                            color: Color::BLACK,
                        },
                    ),
                ));
            });
    });
}
